#include <cassert>
#include <iostream>
#include <vector>

#include "common.hpp"
#include "automorphism.hpp"
#include "matching.hpp"
#include "split.hpp"

// This is the second case in our proof of the main theorem for dimension d=5, namely that there is an even number of matching edges across the cut in the first direction
void run_test() {
    // create automorphisms from permuting all directions except the first direction
    AutoGroup autogroup = AutoGroup::create_automorphisms_from_permutations_of_directions(range(1,DIMENSION));

    // separate vertices in Q^0_1, i.e., with last bit=1
    std::vector<vertex> separated;
    Matching start(MATCH);
    start.set(0, FORBIDDEN);  // mark vertex z=0 as FORBIDDEN
    for(vertex u = 1; u < VERTICES; u+=2) {
        start.set(u, FORBIDDEN);
        separated.push_back(u);
    }

    // compute unextendable matchings in Q^0_0
    Split split(start, separated, autogroup);
    Matchings unextendable = split.get_all_unextendable_matchings_in_main_part();
    // filter out matchings that violate property H in our main theorem in Q^0_0 (we know that they are not extendable)
    filter(unextendable, [](const Matching &m) { return !m.Q0_has_near_half_layers(); });
    std::cout << "The number of non-isomorphic unextendable matchings satisfying property (H) is " << unextendable.size() << "\n";
    // extend those matchings across the cut in direction=0
    unextendable = split.get_all_unextendable_matchings_between_parts(unextendable, true);
    std::cout << "\nThe number of matchings not satisfying the theorem is " << unextendable.size() << "\n";
}

int main() {
    run_test();
    return 0;
}