#include <algorithm>

#include "automorphism.hpp"
#include "matching.hpp"

AutoGroup AutoGroup::create_subgroup_preserving_matchings(const Matching &matching) const {
    AutoGroup autogroup(*this);
    filter(autogroup.automorphisms, [&matching,&autogroup](auto &a){ return matching == apply_automorphism(matching, a); });
    return autogroup;
}

Matching AutoGroup::apply_automorphism(const Matching &matching, const std::array<vertex,VERTICES> &automorphism) {
    Matching r;
    for(vertex u = 0; u < VERTICES; u++) {
        vertex v = matching.get(u);
        r.set(automorphism[u], v < VERTICES ? automorphism[v] : v);
    }
    return r;
}

Matching AutoGroup::get_signature(const Matching &matching) const {
    Matching signature(matching);
    for(const auto &a : automorphisms) {
        Matching s = apply_automorphism(matching, a);
        if(s < signature)
            signature = s;
    }
    return signature;
}

void AutoGroup::remove_isomorphic_matchings(Matchings &matchings) const {
    for(Matching &m : matchings)
        m = get_signature(m);  // replace each matching by its lexicographically least representative

    // sort and filter out unique matchings
    std::sort(matchings.begin(), matchings.end());
    auto ip = std::unique(matchings.begin(), matchings.end());
    matchings.resize(std::distance(matchings.begin(), ip));
}