#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <string>

#include "common.hpp"

vertex get_neighbor(vertex u, int direction) {
    return u ^ (1 << direction);
}

std::array<int,256> create_vertex_weight_table() {
    std::array<int,256> table;
    table[0] = 0;
    for(int i = 0; i < 256; i++)
        table[i] = (i & 1) + table[i / 2];
    return table;
}

bool is_odd(vertex u) {
    return vertex_weight[u] % 2 == 1;
}

size_t get_number_of_matchings(size_t match_vertices, size_t matching_size) {
    assert(2*matching_size <= match_vertices && match_vertices <= double_factorial.size());
    return (2*matching_size == match_vertices) ? double_factorial[match_vertices-1] : double_factorial[match_vertices-1] / double_factorial[match_vertices-2*matching_size-1];
}

std::array<std::string,256> create_vertex_string() {
    std::array<std::string,256> names;
    for(int i = 0; i < 256; i++) 
        names[i] = std::to_string(i);
    names[FORBIDDEN] = "FORBIDDEN";
    names[TERMINAL] = "TERMINAL";
    names[UNCOVERED] = "UNCOVERED";
    names[MATCH] = "MATCH";
    return names;
}

std::string get_running_time() {
    double time = std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - start_time).count() / 1000;
    int hours = std::floor(time/3600);
    time -= 3600 * hours;
    int mins = std::floor(time/60);
    time -= 60 * mins;
    return std::to_string(hours) + ":" + std::to_string(mins) + ":" + std::to_string(time);
}