#include <iostream>

#include "common.hpp"
#include "automorphism.hpp"
#include "extend_all_matchings.hpp"
#include "matching.hpp"
#include "tikz.hpp"

// This function generates all matchings of K(Q_d) (perfect or non-perfect) and extends them to a cycle (avoiding 0 or including it).
// It works in reasonable time for d=2,3,4, but not for d=5.
void run_test(bool forbid_zero, bool only_perfect) {
    // create automorphisms from permuting all directions
    AutoGroup autogroup = AutoGroup::create_automorphisms_from_permutations_of_all_directions();
    Matching empty(MATCH);
    if(forbid_zero)
        empty.set(0, FORBIDDEN);  // mark vertex z=0 as FORBIDDEN
    else
        autogroup.add_automorphisms_by_flipping_all_bits();
    
    Matchings unextendable;
    if(only_perfect) {
        ExtendAllMatchings::run(empty, autogroup, unextendable, true);
    }
    else {
        Matchings matchings = empty.get_all_subsets_of_vertices(UNCOVERED, !forbid_zero);
        autogroup.remove_isomorphic_matchings(matchings);
        for(const Matching &m : matchings) 
            ExtendAllMatchings::run(m, autogroup, unextendable, true);
    }
    autogroup.remove_isomorphic_matchings(unextendable);
    std::cout << "There are " << unextendable.size() << " unextendable matchings\n";
    filter(unextendable, [](const Matching &m) { return m.satisfies_property_H(); });
    std::cout << "There are " << unextendable.size() << " unextendable matchings satisfying property (H)\n";
    std::cout << "Extended paths " << Paths<true>::counter_extensions << ", cycles "  << Paths<false>::counter_extensions << ", recursions " << Paths<false>::counter_recursions + Paths<true>::counter_recursions << ", time " << get_running_time() << "\n";
    if(unextendable.size())
        export_matchings(unextendable);
}

int main() {
    run_test(true, false);  // this gives 11 unextendable matchings in Q_4 (-DDIMENSION=4)
    //run_test(false, false);  // this gives 0 unextendable matchings in Q_2, Q_3, Q_4 (-DDIMENSION=2, 3 or 4)
    return 0;
}