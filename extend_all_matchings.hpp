#pragma once

#include <cassert>

#include "automorphism.hpp"
#include "common.hpp"
#include "matching.hpp"
#include "paths.hpp"

/*
    Given a (vector of) matching(s), generate all matchings that can be obtained by all perfect matchings on MATCH vertices.
    The generated matchings are extended into a Hamilton cycle, and the unextendable matchings are returned.
    The edges are added in a BFS fashion (storing all resulting matchings and reducing by isomorphisms) as long as it is "reasonable"
    (i.e., number of matchings not too large), and by DFS later (backtracking with only a single matching being processed and tested
    at a time).
*/
class ExtendAllMatchings {
    const size_t match_vertices;
    const AutoGroup autogroup;
    const bool verbose;
    Matchings &unextendable;

    ExtendAllMatchings(size_t _match_vertices, const AutoGroup &_autogroup, Matchings &_unextendable, bool _verbose) : match_vertices(_match_vertices), autogroup(_autogroup), verbose(_verbose), unextendable(_unextendable) { }

    Matchings add_reasonable_number_of_edges(const Matchings &matchings) const {
        Matchings extensions{matchings};
        if(verbose)
            std::cout << "Find reasonable number of matchings: match vertices " << match_vertices << "\n";
        // add one edge at the time as long as number of matchings hasn't become too big
        for(size_t i = 1; i <= match_vertices/2 && is_reasonable_to_expand_by_bfs(i, extensions.size()); i++) {
            extensions = Matching::extend_matchings_by_one_edge(extensions, extensions.size()*(match_vertices-2*i+1));
            autogroup.remove_isomorphic_matchings(extensions);  // immediately remove isomorphic matchings
            if(verbose)
                std::cout << "The number of matchings with " << i << " added edges is " << extensions.size() << "\n";
            if(!autogroup.is_empty()) {
                if(verbose) 
                    std::cout << "The number of non-isomorphic matchings is " << extensions.size() << " out of " << get_number_of_matchings(match_vertices, i) << ", so the reduction is " << 1.0*get_number_of_matchings(match_vertices, i)/extensions.size() << " out of " << autogroup.size() << "\n";
            }
        }
        return extensions;
    }

    bool is_reasonable_to_expand_by_bfs(size_t added_edge, size_t number_of_matchings) const {
        if(number_of_matchings * (match_vertices-2*added_edge+1) > (2<<25)) // memory limit
            return false;
        if(1.0*get_number_of_matchings(match_vertices,added_edge-1)/number_of_matchings > 0.8 * autogroup.size()) // automorphism group can no longer speed up the computation
            return false;
        return true;
    }

    // add one additional matching edge with one end vertex u (or the first MATCH vertex larger than u)
    void get_unextendable_perfect_matchings_on_match_vertices_recursion(const Matching &matching, vertex u = 0) {
        while(u < VERTICES && matching.get(u) != MATCH)
            u++;
        assert(matching.find(MATCH) == u || matching.find(MATCH) == NOT_FOUND);

        if(u >= VERTICES) {  // recursion anchor, matching is completed and is tested for extendability
            if(!Paths<false>::is_extendable(matching))
                unextendable.push_back(matching);
            return;
        }

        // find a partner for u
        for(vertex v = u + 1; v < VERTICES; v++) {
            if(matching.get(v) == MATCH) {
                Matching m(matching);
                m.match(u, v);
                get_unextendable_perfect_matchings_on_match_vertices_recursion(m, u+1);
            }
        }
    }

    void get_all_unextendable_matchings(const Matchings &matchings) {
        // first add edges as long as reasonable by BFS
        Matchings extensions = autogroup.is_empty() ? matchings : add_reasonable_number_of_edges(matchings);
        if(verbose)
            std::cout << "Extend " << extensions.size() << " matchings by recursion; " << extensions.size()*extensions[0].get_number_of_perfect_matchings_on_match_vertices() << " matchings to be tested." << "\n";

        // only keep maximal matchings
        filter(extensions, [](const Matching &m) { return m.is_maximal(); });
        // proceed processing matchings in DFS fashion (only one matching at the time is completed and tested for extendability)
        for(const Matching &matching : extensions)
            get_unextendable_perfect_matchings_on_match_vertices_recursion(matching);
        if(verbose)
            std::cout << "The number of unextendable matchings is " << unextendable.size() << " \n";
    }

public:
    static Matchings run(const Matching &start, const AutoGroup &autogroup, bool verbose) {
        Matchings matchings{start}, unextendable;
        run(matchings, autogroup, unextendable, verbose);
        return unextendable;
    }

    static void run(const Matching &start, const AutoGroup &autogroup, Matchings &unextendable, bool verbose) {
        run(Matchings{start}, autogroup, unextendable, verbose);
    }

    static void run(const Matchings &matchings, const AutoGroup &autogroup, Matchings &unextendable, bool verbose) {
        size_t match_vertices = matchings[0].count(MATCH);
        for(const Matching &m : matchings)
            assert(match_vertices == m.count(MATCH));
        ExtendAllMatchings all(match_vertices, autogroup, unextendable, verbose);
        all.get_all_unextendable_matchings(matchings);
    }
};