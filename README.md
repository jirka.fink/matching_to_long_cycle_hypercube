# Matchings in hypercubes extend to long cycles

This repository provides a supplementary codes for the mathematical paper **Matchings in hypercubes extend to long cycles** by [Jiří Fink](https://ktiml.mff.cuni.cz/~fink/) and [Torsten Mütze](http://www.tmuetze.de/index.html) which is available on a (arXiv:2401.01769)[https://arxiv.org/abs/2401.01769].


## Abstract

The d-dimensional hypercube graph Q<sub>d</sub> has as vertices all subsets of {1, ..., d}, and an edge between any two sets that differ in a single element. 
The Ruskey-Savage conjecture asserts that every matching of Q<sub>d</sub>, d ≥ 2, can be extended to a Hamilton cycle, i.e., to a cycle that visits every vertex exactly once.
We prove that every matching of Q<sub>d</sub>, d ≥ 2, can be extended to a cycle that visits at least a 2/3-fraction of all vertices.


## Content

This repository provides source codes which verifies that the base of the induction of theorems proven in the paper.
Use the following commands to compile and run all tests.

    $ make
    $ make run

Note that all tests runs for about 20 hours on a single thread.


## Dependency

* GNU Make 4.3
* g++ 12.2.0
