#pragma once

#include <algorithm>
#include <array>
#include <vector>

#include "common.hpp"
#include "automorphism.hpp"
#include "matching.hpp"

class AutoGroup;
class Matching;
typedef std::vector<Matching> Matchings;

// class that represents the automorphism group (or one of its subgroups) of the hypercube
class AutoGroup {
    // The automorphisms are stored in a vector, and each automorphism is represented as an array
    // that returns the image for each vertex under the automorphism.
    std::vector<std::array<vertex,VERTICES>> automorphisms;

public:
    AutoGroup create_subgroup_preserving_matchings(const Matching &matching) const;

    static AutoGroup create_automorphisms_from_permutations_of_all_directions() {
        return create_automorphisms_from_permutations_of_directions(range<int>(DIMENSION));
    }

    static AutoGroup create_automorphisms_from_permutations_of_directions(std::vector<int> directions) {
        AutoGroup autogroup;
        std::vector<int> permutation(directions);
        do {
            std::vector<int> p = range<int>(DIMENSION);
            for(size_t i = 0; i < directions.size(); i++)
                p[directions[i]] = permutation[i];

            std::array<vertex,VERTICES> a{};
            for(vertex u = 0; u < VERTICES; u++)
                for(int d = 0; d < DIMENSION; d++)
                    a[u] |= (u & (1 << d)) ? (1 << p[d]) : 0;

            autogroup.automorphisms.push_back(a);
        } while(std::next_permutation(permutation.begin(), permutation.end()));
        return autogroup;
    }

    template<class Func>
    void add_automorphisms_by_function(Func func) {
        auto old_count = automorphisms.size();
        automorphisms.resize(2 * old_count);
        std::copy_n(automorphisms.begin(), old_count, automorphisms.begin() + old_count);
        for(size_t i = old_count; i < automorphisms.size(); i++)
            for(vertex u = 0; u < VERTICES; u++)
                automorphisms[i][u] = func(automorphisms[i][u]);
    }

    void add_automorphisms_by_flipping_bits(vertex flip) {
        add_automorphisms_by_function([flip](vertex u) { return u ^ flip; });
    }

    void add_automorphisms_by_flipping_all_bits() {
        for(int d = 0; d < DIMENSION; d++) 
            add_automorphisms_by_flipping_bits(1<<d);
    }
  
    static Matching apply_automorphism(const Matching &matching, const std::array<vertex,VERTICES> &automorphism);
    // the signature of a matching is the lexicographically least matching obtained by applying all automorphisms
    Matching get_signature(const Matching &matching) const;
    // remove matchings that are isomorphic under all automorphisms
    void remove_isomorphic_matchings(Matchings &matchings) const;

    bool is_empty() const {
        return automorphisms.size() <= 1;
    }

    size_t size() const {
        return automorphisms.size();
    }
};