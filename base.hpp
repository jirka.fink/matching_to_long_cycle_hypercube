#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <filesystem>

#include "common.hpp"

// base class for a matching of K(Q_d)
class Base {
    std::array<vertex,VERTICES> matching;

public:
    Base() = default;
    Base(const Base &) = default;
    Base(Base &&) = default;
    Base(std::array<vertex,VERTICES> m) : matching(m) {
        assert(m.size() == VERTICES);
        assert(verify());
    }
    Base(vertex init) {
        for(vertex u = 0; u < VERTICES; u++)
            set(u, init);
    }

    Base& operator=(const Base&) = default;
    bool operator==(const Base &b) const { return matching == b.matching; };
    bool operator<(const Base &b) const { return matching < b.matching; };

    // get partner vertex of u
    vertex get(vertex u) const {
        assert(u < VERTICES);
        return matching[u];
    }

    int count(vertex value) const {
        return std::count(matching.begin(), matching.end(), value);
    }

    int get_remaining_to_connect() const {
        return std::count_if(matching.begin(), matching.end(), [](vertex u) { return u < VERTICES || u == TERMINAL; } );
    }

    int get_matching_size() const {
        return std::count_if(matching.begin(), matching.end(), [](vertex u) { return u < VERTICES; } ) / 2;
    }

    void set(vertex u, vertex v) {
        assert(u < VERTICES);
        assert(v < VERTICES || v == FORBIDDEN || v == TERMINAL || v == UNCOVERED || v == MATCH);
        matching[u] = v;
    }

    void match(vertex u, vertex v) {
        set(u, v);
        set(v, u);
    }
  
    // find first vertex with the desired label
    vertex find(vertex value) const {
        for(vertex u = 0; u < VERTICES; u++)
            if(get(u) == value)
                return u;
        return NOT_FOUND;
    }

    // check consistency of the matching
    bool verify() const {
        for(vertex u = 0; u < VERTICES; u++) {
            vertex v = get(u);
            if(v < VERTICES) {  // normal vertex label
                if (get(v) != u) return false;  // matching must go both ways
                if (u == v) return false;  // no vertex matched to itself
            }
            else
                if (v != FORBIDDEN && v != TERMINAL && v != UNCOVERED && v != MATCH) return false;  // only special vertex labels allowed
        }
        if (count(TERMINAL) % 2 == 1) return false;  // number of terminals must be even
        return true;
    }

    void print() const {
        for(vertex u = 0; u < VERTICES; u++) {
            std::cout << "   " << int(u) << "  " << vertex_string[get(u)] << "\n";
        }
    }
};