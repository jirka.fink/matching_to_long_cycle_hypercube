#include <iostream>

#include "common.hpp"
#include "automorphism.hpp"
#include "extend_all_matchings.hpp"
#include "matching.hpp"

// This is the first case in our proof of the main theorem for dimension d=5, namely that there is a (near) half-layer in the first direction
void run_test() {
    // create automorphisms from permuting all directions except the first direction
    AutoGroup autogroup = AutoGroup::create_automorphisms_from_permutations_of_directions(range(1,DIMENSION));
    std::cout << "The size of the automorphism group is " << autogroup.size() << "\n";

    // create the half-layer in direction 0
    Matching start(MATCH);
    start.set(0, FORBIDDEN);  // mark vertex z=0 as FORBIDDEN
    for(vertex u = 1; u < VERTICES; u++)
        if(is_odd(u) && (u & 1) == 0)
            start.match(u, get_neighbor(u, 0));
  
    // remove one of the edges from the half-layer
    // (matchings containing the half-layer will be created again by completing the matching)
    Matchings matchings = Matching::remove_one_edge_from_all_matchings(Matchings({start}), MATCH);
  
    std::cout << "The number of types of matchings to be tested is " << matchings.size() << "\n";

    autogroup.remove_isomorphic_matchings(matchings);
    std::cout << "The number of non-isomorphic types is " << matchings.size()  << "\n";

    Matchings unextendable;
    for(Matching &m : matchings) {
        Matchings subsets = m.get_all_subsets_of_vertices(UNCOVERED, false);
        std::cout << "The number of subsets of uncovered vertices is " << subsets.size() << "\n";

        autogroup.remove_isomorphic_matchings(subsets);
        std::cout << "The number of non-isomorphic subsets of uncovered vertices is " << subsets.size()  << "\n";

        for(Matching &s : subsets)
            ExtendAllMatchings::run(s, autogroup.create_subgroup_preserving_matchings(s), unextendable, false);
    }

    std::cout << "The number of unextendable matchings is " << unextendable.size() << "\n";
    // filter out matchings that violate property H in our main theorem (we know that they are not extendable)
    filter(unextendable, [](const Matching &m) { return m.satisfies_property_H(); });
    std::cout << "Extended paths " << Paths<true>::counter_extensions << ", cycles "  << Paths<false>::counter_extensions << ", recursions " << Paths<false>::counter_recursions + Paths<true>::counter_recursions << ", time " << get_running_time() << "\n";
    std::cout << "\nThe number of matchings not satisfying the theorem is " << unextendable.size() << "\n";
}

int main() {
    run_test();
    return 0;
}