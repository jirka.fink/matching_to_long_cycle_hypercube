#pragma once

#include <string>

#include "matching.hpp"

void export_matchings_to_tikz(const Matchings &matchings, std::string filename = "results.tex");
void export_matching_to_tikz(const Matching &matching, std::string filename = "results.tex");
void export_matchings_to_txt(const Matchings &matchings, std::string filename);
void export_matchings(const Matchings &matchings, std::string filename = "result");
Matchings import_matchings(std::string filename);