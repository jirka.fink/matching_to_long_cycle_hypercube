#include <cassert>
#include <iostream>
#include <vector>

#include "automorphism.hpp"
#include "matching.hpp"

Matchings Matching::extend_matchings_between_terminals_and_separated(const Matchings &matchings, const std::vector<vertex> separated, const AutoGroup &autogroup, bool verbose) {
    size_t terminals = matchings[0].count(TERMINAL);
    Matchings extensions(matchings);

    // go over number of vertices labeled as TERMINALs
    for(size_t it = 0; it < terminals; it++) {
        size_t expected_count = extensions.size() * (separated.size()-it);
        Matchings extend{std::move(extensions)};
        extensions.reserve(expected_count);

        for(const Matching &matching : extend) {
            vertex u = matching.find(TERMINAL);  // find first TERMINAL vertex
            // go over all separated vertices labeled as FORBIDDEN           
            for(vertex v : separated)
                if(matching.get(v) == FORBIDDEN) {
                    Matching m(matching);
                    m.match(u, v);  // match them up
                    extensions.push_back(m);
                }
        }
        assert(extensions.size() == expected_count);

        if(verbose)
            std::cout << "The number of matchings after adding " << it+1 << " edges between parts is " << extensions.size() << "\n";
        autogroup.remove_isomorphic_matchings(extensions);
        if(verbose)
            std::cout << "The number of non-isomorphic matchings is " << extensions.size() << "\n";
    }
    return extensions;
}