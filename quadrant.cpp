#include <cassert>
#include <iostream>
#include <vector>

#include "common.hpp"
#include "automorphism.hpp"
#include "matching.hpp"
#include "split.hpp"
#include "tikz.hpp"

bool Q0_has_halflayer(const Matching &m) {
    assert(m.get(0) == FORBIDDEN);
    for(int d = 0; d < DIMENSION-1; d++) {
        bool has_halflayer = true;
        for(vertex u = 1; u < VERTICES/2; u++)
            if((u & (1<<d)) == 0 && is_odd(u) && m.get(u) != get_neighbor(u, d)) {
                has_halflayer = false;
                break;
            }
        if(has_halflayer)
            return true;
    }
    return false;
}

bool has_halflayer_to_separated(const Matching &m, const std::vector<vertex> &kept) {
    for(vertex u : kept)
        if(!is_odd(u) && m.get(u) != get_neighbor(u, DIMENSION-1))
            return false;
    return true;
}

// this function is the same as run_test(), but instead of computing the unextendable matchings in the first step,
// it imports them from a file (produced by an initial run of run_test(), which writes them to a file)
void run_second(const std::vector<vertex> &separated, const std::vector<vertex> &kept, std::string filename) {
    AutoGroup autogroup = AutoGroup::create_automorphisms_from_permutations_of_directions({range(DIMENSION-2)});

    Matching start(MATCH);
    start.set(0, FORBIDDEN);
    for(vertex u : separated) {
        start.set(u, FORBIDDEN);
    }
    
    Split split(start, separated, autogroup);
    Matchings unextendable = import_matchings(filename);
    std::cout << "The number of imported matchings is " << unextendable.size() << "\n"; 

    filter(unextendable, [](const Matching &m) { return !Q0_has_halflayer(m); });
    std::cout << "The number of matchings without half-layer in Q^i_0 " << unextendable.size() << "\n"; 

    filter(unextendable, [&kept](const Matching &m) { return !has_halflayer_to_separated(m, kept); });
    std::cout << "The number of matchings without the quad-layer between Q^i_0 and the kept quadrant of Q^i_1 " << unextendable.size() << "\n"; 

    unextendable = split.get_all_unextendable_matchings_between_parts(unextendable, false);
    std::cout << "The number of unextendable matchings " << unextendable.size() << "\n";
    filter(unextendable, [](const Matching &m) { return m.satisfies_property_H(); });
    std::cout << "\nThe number of matchings not satisfying the theorem is " << unextendable.size() << "\n";
}

// This is the third case in our proof of the main theorem for dimension d=5, namely that there is an odd number of matching edges across the cut in the last direction.
// Unlike in the first two parts of the code, we take the split direction i to be the last direction (i=d-1), and we take
// the secondary split direction j to be the second-to-last direction (i=d-2). This has some advantages when running the
// matching generation via BFS.
void run_test(const std::vector<vertex> &separated, const std::vector<vertex> &kept, std::string filename) {
    // create automorphisms from permuting all directions except the last two
    AutoGroup autogroup = AutoGroup::create_automorphisms_from_permutations_of_directions({range(DIMENSION-2)});

    // separate vertices
    Matching start(MATCH);
    start.set(0, FORBIDDEN);
    for(vertex u : separated) {
        start.set(u, FORBIDDEN);
    }

    // compute unextendable matchings in Q_{10}' or Q_{11}'
    Split split(start, separated, autogroup);
    Matchings unextendable = split.get_all_unextendable_matchings_in_main_part(true);
    std::cout << "The number of unextendable matchings " << unextendable.size() << "\n";

    export_matchings_to_txt(unextendable, filename);

    // filter out matchings that contain half-layers in Q_0 or the special half-layer in direction i towards
    // the kept (i.e., non-removed) quadrant of the whole cube
    filter(unextendable, [](const Matching &m) { return !Q0_has_halflayer(m); });
    std::cout << "The number of matchings without half-layer in Q^i_0 " << unextendable.size() << "\n"; 

    filter(unextendable, [&kept](const Matching &m) { return !has_halflayer_to_separated(m, kept); });
    std::cout << "The number of matchings without the quad-layer between Q^i_0 and the kept quadrant of Q^i_1 " << unextendable.size() << "\n"; 

    // extend those matchings towards the removed quadrant
    unextendable = split.get_all_unextendable_matchings_between_parts(unextendable, false);
    std::cout << "The number of unextendable matchings " << unextendable.size() << "\n";
    filter(unextendable, [](const Matching &m) { return m.satisfies_property_H(); });
    std::cout << "\nThe number of matchings not satisfying the theorem is " << unextendable.size() << "\n";
}

int main() {
    // the separated set of vertices has the two most significant bits 10 or 11, respectively
    const std::vector<vertex>
        separated_Q10 { 0b10000, 0b10001, 0b10010, 0b10011, 0b10100, 0b10101, 0b10110, 0b10111 },
        separated_Q11 { 0b11000, 0b11001, 0b11010, 0b11011, 0b11100, 0b11101, 0b11110, 0b11111 };

#if QUADRANT == 11
    run_test(separated_Q10, separated_Q11, "../results/unextendable_Q10.txt");
#elif QUADRANT == 10
    run_test(separated_Q11, separated_Q10, "../results/unextendable_Q11.txt");
#else
#error QUADRANT must be 11 or 10
#endif

    return 0;
}
