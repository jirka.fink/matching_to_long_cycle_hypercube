#pragma once

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include "automorphism.hpp"
#include "base.hpp"
#include "common.hpp"

class AutoGroup;
class Matching;
typedef std::vector<Matching> Matchings;

// This class provides basic routines for extending matchings into matchings, e.g., generating all (perfect) matchings with some properties.
class Matching : public Base {
public:
    Matching() = default;
    Matching(const Matching &) = default;
    Matching(Matching &&) = default;
    Matching(std::array<vertex,VERTICES> m) : Base(m) {}
    Matching(vertex init) : Base(init) {}

    Matching& operator=(const Matching&) = default;
  
    /*
        Return all matchings obtained from the given matchings by adding one vertex (u if it is labeled as MATCH)
        with all other MATCH vertices. The number of resulting matchings is required (for checking consistency).
    */
    static Matchings extend_matchings_by_one_edge(Matchings &matchings, size_t expected_count, vertex u = 0) {
        Matchings extensions;
        extensions.reserve(expected_count);
        for(Matching &m : matchings)
            m.extend_matching_by_one_edge(extensions, u);
        assert(extensions.size() == expected_count);
        return extensions;
    }

    /*
        Return all matchings obtained from the given matchings by adding one vertex (u if its is labeled as MATCH)
        with all other MATCH vertices. Requires all matchings to have the same number of MATCH vertices.
    */
    static Matchings extend_matchings_by_one_edge(Matchings &matchings, vertex u = 0) {
        return extend_matchings_by_one_edge(matchings, matchings.size() * (matchings[0].count(MATCH)-1), u);
    }

    /*
        Create all matchings obtained by extending this matching by one edge.
        The added edge joins one vertex (u if it is labeled as MATCH) with all other MATCH vertices.
        The resulting matchings are returned via the extensions variable.
    */
    void extend_matching_by_one_edge(Matchings &extensions, vertex u = 0) {
        u = get(u) == MATCH ? u : find(MATCH);
        assert(u < VERTICES);
        for(vertex v = 0; v < VERTICES; v++) {            
            if(get(v) == MATCH && u != v) {
                Matching m(*this);
                m.match(u, v);
                assert(m.verify());
                extensions.push_back(m);
            }
        }
    }

    /*
        Return all subsets of vertices obtained by relabeling some subset of MATCH vertices by the new_value
        (the remaining MATCH vertices are not yet matched to each other).
        If even_subsets is true, an even number of vertices is relabeled; otherwise, an odd number.
    */
    Matchings get_all_subsets_of_vertices(vertex new_value, bool even_subsets) {
        const size_t match_vertices = count(MATCH);
        const size_t expected_count = match_vertices == 0 ? 1 : (1 << (match_vertices-1));
        Matchings extensions;
        extensions.reserve(expected_count);
        get_all_subsets_of_vertices_recursion(extensions, new_value, 0, !even_subsets);
        if(even_subsets)
            extensions.push_back(*this);  // include empty subset
        assert(extensions.size() == expected_count);
        return extensions;
    }

    void get_all_subsets_of_vertices_recursion(Matchings &extensions, vertex new_value, vertex u, bool parity) {
        for(; u < VERTICES; u++)
            if(get(u) == MATCH) {
                Matching m(*this);
                m.set(u, new_value);
                if(parity) {
                    assert(m.verify());
                    extensions.push_back(m);
                }
                m.get_all_subsets_of_vertices_recursion(extensions, new_value, u+1, !parity);
            }
    }

    /*
        Return all matchings obtained by relabeling some even subset of separated MATCH vertices by the new label UNCOVERED.
    */
    Matchings get_all_subsets_of_separated_vertices(const std::vector<vertex> &separated) const {
        const size_t match_vertices = std::count_if(separated.begin(), separated.end(), [this](vertex u) { return get(u) == MATCH; });
        const size_t expected_count = match_vertices == 0 ? 1 : (1 << (match_vertices-1));
        Matchings extensions;
        extensions.reserve(expected_count);
        get_all_subsets_of_separated_vertices_recursion(extensions, separated, 0, false);
        extensions.push_back(*this);  // include empty subset
        assert(extensions.size() == expected_count);
        return extensions;
    }

    void get_all_subsets_of_separated_vertices_recursion(Matchings &extensions, const std::vector<vertex> &separated, vertex u, bool parity) const {
        for(; u < separated.size(); u++)
            if(get(separated[u]) == MATCH) {
                Matching m(*this);
                m.set(separated[u], UNCOVERED);
                if(parity) {
                    assert(m.verify());
                    extensions.push_back(m);
                }
                m.get_all_subsets_of_separated_vertices_recursion(extensions, separated, u+1, !parity);
            }
    }

    /*
        Return all matchings obtained by removing each possible edge from each of the given matchings
        (so the resulting matchings have one less edge).
        The end vertices of the removed edge are labeled by the new_value (TERMINAL by default).
    */
    static Matchings remove_one_edge_from_all_matchings(const Matchings &matchings, vertex new_value = TERMINAL) {
        size_t expected_count = std::accumulate(matchings.begin(), matchings.end(), size_t(0), [](size_t n, const Matching &m) { return n + m.get_matching_size(); });
        Matchings created;
        created.reserve(expected_count);
        for(const Matching &matching : matchings)
            for(vertex u = 0; u < VERTICES; u++)
                if(matching.get(u) < u) {  // remove the edge starting at the higher end vertex
                    Matching m(matching);
                    m.set(u, new_value);
                    m.set(matching.get(u), new_value);
                    assert(m.verify());
                    created.push_back(m);
                }
        assert(expected_count == created.size());
        return created;
    }

    /*
        Return all matchings obtained by adding all possible edges between all vertices labeled as TERMINAL and some separated vertices labeled as FORBIDDEN.
        All matchings are expected to have the same number of terminals.
    */
    static Matchings extend_matchings_between_terminals_and_separated(const Matchings &matchings, const std::vector<vertex> separated, const AutoGroup &autogroup, bool verbose);
  
    /*
        Check if the matching satisfies property (H) of our main theorem, namely that if it contains a half-layer in direction d,
        then there must be a second vertex u (in addition to z=0) with u_d=0 that is uncovered (unmatched)
    */
    bool satisfies_property_H() const {
        assert(get(0) == FORBIDDEN);
        for(int d = 0; d < DIMENSION; d++) {
            bool ok = false;
            for(vertex u = 1; u < VERTICES; u++) {
                if((u & (1<<d)) == 0) {  // u_d == 0
                    if(get(u) == UNCOVERED) {  // u is unmatched
                        ok = true;
                        break;
                    }
                    if(get(u) != get_neighbor(u, d) && is_odd(u)) {  // the edge (u,u^d) of the half-layer is not present in the matching
                        ok = true;
                        break;
                    }
                }
            }
            if(!ok)
                return false;
        }
        return true;
    }
  
    /*
        Check if the submatching in Q^0_0 has no (near) half-layers.
    */
    bool Q0_has_near_half_layers() const {
        assert(get(0) == FORBIDDEN);
        for(int d = 1; d < DIMENSION; d++) {  // don't consider dimension=0
            int missing = 0;  // count number of missing half-layer edges
            for(vertex u = 2; u < VERTICES; u+=2) {  // only consider vertices with last bit=0
                if(((u & (1<<d)) == 0) && is_odd(u)) {
                    if(get(u) == UNCOVERED) {
                        missing++;
                    }
                    else {
                        vertex v = get(u);
                        vertex w = get_neighbor(u, d);
                        if(v != w && v != FORBIDDEN && get(w) != FORBIDDEN) {
                            missing++;
                        }
                    }
                }
            }
            if(missing <= 1)  // 0=half-layer, 1=near half-layer
                return true;
        }
        return false;
    }
 
    /*
        Returns true if the matching is maximal, i.e., if there are no two neighbors marked as UNCOVERED
        (z=0 is marked as FORBIDDEN).
    */
    bool is_maximal() const {
        for(vertex u = 0; u < VERTICES; u++)
            if(get(u) == UNCOVERED)
                for(int d = 0; d < DIMENSION; d++)
                    if(get(get_neighbor(u, d)) == UNCOVERED)
                        return false;
        return true;
    }

    bool has_odd_number_of_edges_in_the_most_significant_direction() const {
        int cnt = 0;  // count the number of covered vertices
        for(vertex u = 0; u < VERTICES/2; u++)
            if(get(u) == MATCH || get(u) < VERTICES)
                cnt++;
        return cnt % 2;
    }

    size_t get_number_of_perfect_matchings_on_match_vertices() const {
        size_t match = count(MATCH);
        return match > 0 ? double_factorial[match-1] : 1;
    }

    std::string to_string() const {
        std::string str = vertex_string[get(0)];
        for(vertex u = 1; u < VERTICES; u++)
           str += ", " + vertex_string[get(u)];
        return str;
    }
};
