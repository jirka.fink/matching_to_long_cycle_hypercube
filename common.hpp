#pragma once

#include <array>
#include <chrono>
#include <limits>
#include <string>
#include <vector>

typedef uint8_t vertex;

// #########################################################################
// DIMENSION has to be defined as a preprocessor variable during compilation
// #########################################################################

// total number of vertices
const vertex VERTICES = 1 << DIMENSION;

// status of vertices during the matching generation
const vertex NOT_FOUND = std::numeric_limits<vertex>::max(); 
const vertex FORBIDDEN = NOT_FOUND - 1;  // do not cover by matching nor cycle
const vertex TERMINAL = FORBIDDEN - 1;  // terminal vertex of a path, do not cover by a matching
const vertex UNCOVERED = TERMINAL - 1;  // do not cover by a matching, can be used in a cycle
const vertex MATCH = UNCOVERED - 1;  // needs to be matched, generate all possible perfect matchings on these vertices

// compute the neighbor of the given vertex obtained by flipping the bit in the given direction
vertex get_neighbor(vertex u, int direction);

// table that contains the Hamming weight (=# of 1s) for each vertex
std::array<int,256> create_vertex_weight_table();
const std::array<int,256> vertex_weight = create_vertex_weight_table();

// returns true iff vertex has odd Hamming weight
bool is_odd(vertex u);

// precomputed double factorial numbers
const std::array<size_t,34> double_factorial = { 1, 1, 2, 3, 8, 15, 48, 105, 384, 945, 3840, 10395, 46080, 135135, 645120, 2027025, 10321920, 34459425, 185794560, 654729075, 3715891200, 13749310575, 81749606400, 316234143225, 1961990553600, 7905853580625, 51011754393600, 213458046676875, 1428329123020800, 6190283353629375, 42849873690624000, 191898783962510625, 1371195958099968000, 6332659870762850625 };

// compute the number of possible matchings with the given number of edges on the given number of vertices
size_t get_number_of_matchings(size_t match_vertices, size_t matching_size);

std::array<std::string,256> create_vertex_string();
const std::array<std::string,256> vertex_string = create_vertex_string();

// generate a range of objects [start,end[, i.e., including the start index, but excluding the end index
template<typename T>
std::vector<T> range(T start, T step, T end) {
    std::vector<T> result;
    result.reserve((end-start)/step);
    for(T i = start; i < end; i += step)
        result.push_back(i);
    return result;
}

template<typename T> std::vector<T> range(T start, T end) { return range<T>(start, 1, end); }
template<typename T> std::vector<T> range(T end) { return range<T>(0, 1, end); }

// filter objects by applying given function, keep only the ones for which the function returns true
template<typename T, typename Func>
void filter(std::vector<T> &v, Func f) {
    size_t j = 0;
    for(size_t i = 0; i < v.size(); i++)
        if(f(v[i])) {
            v[j] = std::move(v[i]);
            j++;
        }
    v.resize(j);
}

const auto start_time = std::chrono::high_resolution_clock::now();
// compute elapsed time since starting the program
std::string get_running_time();