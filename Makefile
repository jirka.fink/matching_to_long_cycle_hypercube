CC=g++
FLAGS=-std=c++17 -O3 -DNDEBUG    # -ggdb3 -MMD -Wall -Wextra -Wpedantic -DDEBUG
SOURCES1=common.cpp automorphism.cpp matching.cpp tikz.cpp brute_force.cpp
SOURCES2=common.cpp automorphism.cpp matching.cpp halflayer.cpp
SOURCES3=common.cpp automorphism.cpp matching.cpp even_edges_in_layer.cpp
SOURCES4=common.cpp automorphism.cpp matching.cpp tikz.cpp quadrant.cpp

all:
	$(CC) $(FLAGS) -DDIMENSION=4 $(SOURCES1) -o test1
	$(CC) $(FLAGS) -DDIMENSION=5 $(SOURCES2) -o test2
	$(CC) $(FLAGS) -DDIMENSION=5 $(SOURCES3) -o test3
	$(CC) $(FLAGS) -DDIMENSION=5 -DQUADRANT=10 $(SOURCES4) -o test4
	$(CC) $(FLAGS) -DDIMENSION=5 -DQUADRANT=11 $(SOURCES4) -o test5

clean:
	rm -f *.o test1 test2 test3 test4 test5

run:
	./test1
	./test2
	./test3
	./test4
	./test5
