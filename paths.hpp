#pragma once

#include <cassert>

#include "common.hpp"
#include "base.hpp"
#include "matching.hpp"

// This class extend a matching into a linear forest (if HAS_TERMINALS is true) or a cycle (if HAS_TERMINALS is false).
// The extension is maintained in contracted form, i.e., every path is immediately replaced by the shortcut edge that
// connects the two end vertices of the path.
template <bool HAS_TERMINALS>
class Paths : public Base {
    // For each vertex u, degree[u] gives the number of neighbors of u along hypercube edges that are not forbidden,
    // i.e., that are possible edges for the extensions. In the recursion, we always proceed extending vertices of
    // minimum degree, with the hope that most decisions will be forced (degree=1).
    std::array<vertex,VERTICES> degree;
#ifdef DEBUG
    // For debugging purposes, all edges of the extending cycle or linear forest (including the prescribed
    // matching edges) can be stored. However, this makes the computation significantly slower.
    // The extension of a matching can be tested by the function verify_extension().
    vertex paths[VERTICES][2];
#endif
int number_of_edges, number_of_terminals;  // number of (contracted) edges still remaining to be extended; number of terminals still remaining to be extended

public:
    // counters for statistical purposes
    static size_t counter_extensions, counter_recursions;

    Paths(const Paths &) = default;

    Paths(const Matching &m) : Base(m), number_of_edges(m.get_matching_size()), number_of_terminals(m.count(TERMINAL)) {
        // initialize degrees
        degree.fill(0);
        for(vertex u = 0; u < VERTICES; u++)
            if(get(u) != FORBIDDEN)
                for(int d = 0; d < DIMENSION; d++)
                    degree[get_neighbor(u, d)]++;
        for(vertex u = 0; u < VERTICES; u++)
            if(get(u) == FORBIDDEN || get(u) == UNCOVERED)
                degree[u] = std::numeric_limits<vertex>::max();

#ifdef DEBUG
        for(vertex u = 0; u < VERTICES; u++) {
            paths[u][0] = get(u);
            paths[u][1] = UNCOVERED;
        }
#endif        
    }
   
    vertex find_vertex_to_extend() const {
        // extend from minimum degree vertex
        auto result = std::min_element(degree.begin(), degree.end());
        if(*result > DIMENSION)
            return NOT_FOUND;
        return std::distance(degree.begin(), result);
    }

    void set_forbidden(vertex u) { 
        set(u, FORBIDDEN);
        degree[u] = std::numeric_limits<vertex>::max();
        for(int d = 0; d < DIMENSION; d++)
            degree[get_neighbor(u, d)]--;
    }

    void set_degree(vertex u) {
        degree[u] = 0;
        for(int d = 0; d < DIMENSION; d++)
            if(get(get_neighbor(u, d)) != FORBIDDEN)
                degree[u]++;
    }

    // join the paths ending in vertices u and v by adding the edge uv
    void join(vertex u, vertex v);
    
    /*
        Extend this matching into a cycle avoiding all FORBIDDEN vertices.
        If the matching contains TERMINAL vertices, extend the matching into a set of vertex-disjoint
        paths terminating in those vertices. The returned object is allocated on the heap.
    */
    Paths* extend_to_paths() const {
#ifdef DEBUG
        for(vertex x = 0; x < VERTICES; x++)
            if(get(x) == FORBIDDEN || get(x) == UNCOVERED)
                assert(degree[x] > VERTICES);
            else {
                int z = 0;
                for(int d = 0; d < DIMENSION; d++)
                    if(get(get_neighbor(x, d)) != FORBIDDEN)
                        z++;
                assert(z == degree[x]);
            }
        assert(number_of_edges == get_matching_size() && number_of_terminals == count(TERMINAL));
        assert(count(MATCH) == 0);
#endif

        counter_recursions++;
        vertex u = find_vertex_to_extend();
        
        for(int d = 0; d < DIMENSION; d++) {
            // find a neighbor v of u to add the edge uv
            vertex v = get_neighbor(u, d);
            if(get(v) == FORBIDDEN)
                continue;

            bool is_last_edge = false;
            if(get(u) == v) {
                if(HAS_TERMINALS || number_of_edges > 1)
                    continue;
                is_last_edge = true;
            }
            if(HAS_TERMINALS && get(u) == TERMINAL && get(v) == TERMINAL && number_of_terminals == 2) {
                if(number_of_edges > 0)
                    continue;
                is_last_edge = true;
            }
            if(is_last_edge) {
                // recursion anchor, extension is complete
                Paths *extension = new Paths(*this);
                extension->join(u, v);
                assert(extension->number_of_edges == 0 && extension->number_of_terminals == 0);
                return extension;
            }

            Paths p(*this);
            p.join(u, v);
            Paths *extension = p.extend_to_paths();
            if(extension)
                return extension;  // returns once the first extension is found
        }
        return nullptr;  // no extension exists
    }

    static bool is_extendable(const Matching &m) {
        assert(m.count(MATCH) == 0);  // no more vertices should be left to be matched
        assert(HAS_TERMINALS == (m.count(TERMINAL) > 0));
        m.verify();

        counter_extensions++;
        Paths *e = Paths(m).extend_to_paths();
        if(!e)
            return false;
        e->verify_extension(m);
        delete e;
        return true;
    }

    void add_edge_to_paths(vertex u, vertex v) {
#ifdef DEBUG
        assert(paths[u][1] > VERTICES && paths[v][1] > VERTICES);
        paths[u][paths[u][0] < VERTICES ? 1 : 0] = v;
        paths[v][paths[v][0] < VERTICES ? 1 : 0] = u;
        assert(paths[u][1] == UNCOVERED || get(u) == FORBIDDEN);
        assert(paths[v][1] == UNCOVERED || get(v) == FORBIDDEN);
#endif
    }

    void traverse(vertex start, bool *visited) const {
#ifdef DEBUG        
        vertex previous = start, current = paths[start][0];
        assert(!visited[start]);
        visited[start] = true;
        while(current != start && current != UNCOVERED) {
            assert(!visited[current]);
            visited[current] = true;
            vertex next = paths[current][paths[current][0] == previous ? 1 : 0];
            previous = current;
            current = next;
        }
#endif
    }

    void verify() const {
#ifdef DEBUG        
        Base::verify();
#endif        
    }

    void verify_extension(const Matching &m) const {
        m.verify();
        verify();
        assert(HAS_TERMINALS == (m.count(TERMINAL) > 0));
        bool visited[VERTICES];

        for(vertex u = 0; u < VERTICES; u++) {
            visited[u] = false;
            vertex v = m.get(u), w = get(u);
            switch(v) {
                case FORBIDDEN:
                    assert(w == FORBIDDEN);
                    assert(paths[u][0] == FORBIDDEN && paths[u][1] == UNCOVERED);
                    break;
                case TERMINAL:
                    assert(w == FORBIDDEN);
                    assert(paths[u][0] < VERTICES && paths[u][1] == UNCOVERED);
                    break;
                case UNCOVERED:
                    if(w == UNCOVERED)
                        assert(paths[u][0] == UNCOVERED && paths[u][1] == UNCOVERED);
                    else
                        assert(w == FORBIDDEN && paths[u][0] < VERTICES && paths[u][1] < VERTICES);
                    break;
                case MATCH:
                    assert(0);
                default:
                    assert(v < VERTICES);
                    assert(w == FORBIDDEN);
                    assert(paths[u][0] == v && paths[u][1] < VERTICES);
            }
        }

        if(m.count(TERMINAL)) {
            for(vertex u = 0; u < VERTICES; u++)
                if(m.get(u) == TERMINAL && !visited[u])
                    traverse(u, visited);  // traverse every path of the linear forest
        }
        else
            for(vertex u = 0; u < VERTICES; u++)
                if(m.get(u) < VERTICES) {
                    traverse(u, visited);  // traverse the single cycle
                    break;
                }
#ifdef DEBUG                
        for(vertex u = 0; u < VERTICES; u++)
            if(paths[u][0] < VERTICES)
                assert(visited[u] != (paths[u][0] == UNCOVERED || paths[u][0] == FORBIDDEN));
#endif
    }
};

template <bool HAS_TERMINALS> size_t Paths<HAS_TERMINALS>::counter_extensions = 0;
template <bool HAS_TERMINALS> size_t Paths<HAS_TERMINALS>::counter_recursions = 0;

// join vertices u and v and immediately replace resulting paths by shortcut edges
template<> void Paths<false>::join(vertex u, vertex v) {
    assert(u < VERTICES && v < VERTICES && get(u) < VERTICES);
    vertex gu = get(u), gv = get(v);
    if(gv == UNCOVERED) {
        // path gu--u--v is replaced by shortcut gu--v
        set(gu, v);
        set(v, gu);
        set_degree(v);
    }
    else {
        // path gu--u--v--gv is replaced by shortcut gu--gv
        set(gu, gv);
        set(gv, gu);
        set_forbidden(v);  // exclude shortcut vertex v
        number_of_edges--;
    }
    set_forbidden(u);  // exclude shortcut vertex u
    add_edge_to_paths(u, v);
}

template<> void Paths<true>::join(vertex u, vertex v) {
    vertex gu = get(u), gv = get(v);
    assert(gu < VERTICES || gu == TERMINAL);
    assert(gv < VERTICES || gv == TERMINAL || gv == UNCOVERED);
    assert(gu != v);

    if(gv < VERTICES)
        set(gv, gu);
    set_forbidden(u);

    if(gv == UNCOVERED) {
        if(gu < VERTICES)
            set(gu, v);
        if(gv == UNCOVERED)
            set_degree(v);
        set(v, gu);
    }
    else { // gv == TERMINAL || gv < VERTICES
        if(gu < VERTICES)
            set(gu, gv);
        set_forbidden(v);
        if(gu == TERMINAL && gv == TERMINAL)
            number_of_terminals -= 2;
        else
            number_of_edges--;
    }
    add_edge_to_paths(u, v);
}
