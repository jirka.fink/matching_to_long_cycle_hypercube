#include <filesystem>
#include <fstream>
#include <string>

#include "common.hpp"
#include "matching.hpp"
#include "tikz.hpp"

void export_matchings_to_tikz(const Matchings &matchings, std::string filename /*= "results.tex"*/) {
    std::vector<std::tuple<std::string,int,int>> nodes = {
        std::make_tuple("000",-3,3),
        std::make_tuple("001",3,3),
        std::make_tuple("010",-3,-3),
        std::make_tuple("011",3,-3),
        std::make_tuple("100",-1,1),
        std::make_tuple("101",1,1),
        std::make_tuple("110",-1,-1),
        std::make_tuple("111",1,-1)
    };
    if(DIMENSION >= 4) {
        for(vertex u = 0; u < 8; u++) {
            nodes.push_back(std::make_tuple("1" + std::get<0>(nodes[u]), 9 - std::get<1>(nodes[u]), std::get<2>(nodes[u])));
            std::get<0>(nodes[u]) = "0" + std::get<0>(nodes[u]);
        }
    }
    if(DIMENSION >= 5) {
        for(vertex u = 0; u < 16; u++) {
            nodes.push_back(std::make_tuple("1" + std::get<0>(nodes[u]), std::get<1>(nodes[u]), - 9 - std::get<2>(nodes[u])));
            std::get<0>(nodes[u]) = "0" + std::get<0>(nodes[u]);
        }
    }

    std::ofstream file(filename, std::ios::out);
    file <<
"\\documentclass{article}\n\
\\usepackage[utf8]{inputenc}\n\
\\usepackage[a4paper,top=1cm,left=1cm]{geometry}\n\
\\usepackage{tikz,pgfplots}\n\
\\usetikzlibrary{shapes.geometric,shapes.multipart,shapes.misc,positioning,decorations.pathreplacing,plotmarks,patterns,calc,matrix}\n\
\\tikzset{\n\
    every node/.style = { draw, circle }\n\
}\n\
\\begin{document}\n\
\\noindent\n";

    for(const Matching& m : matchings) {
        file << "\n\n\\begin{tikzpicture}\n\\draw (2.5cm,2.5cm) {};\n";
        for(vertex u = 0; u < VERTICES; u++) {
            std::string style = "";
            if(m.get(u) == FORBIDDEN)
                style = "[fill=red]";
            else if(m.get(u) == TERMINAL)
                style = "[fill=blue]";
            file << "\\draw (" << std::get<1>(nodes[u]) << "cm," << std::get<2>(nodes[u]) << "cm) node " << style <<  " (" << int(u) << ") {" << std::get<0>(nodes[u]) << "};\n";
        }
        for(vertex u = 0; u < VERTICES; u++) {
            if(m.get(u) < u)
                file << "\\path (" << int(m.get(u)) << ") edge [bend right] (" << int(u) << ");\n";
        }
        file << "\\end{tikzpicture}\n\n" << m.to_string() << "\n\n";
    }
    file << "\\end{document}\n";
    file.close();
}

void export_matching_to_tikz(const Matching &matching, std::string filename /*= "results.tex"*/) {
    export_matchings_to_tikz(Matchings({matching}), filename);
}

void export_matchings_to_txt(const Matchings &matchings, std::string filename) {
    std::ofstream file(filename, std::ios::out);
    for(const Matching &m : matchings) {
        for(vertex u = 0; u < VERTICES; u++)
            file << int(m.get(u)) << " ";
        file << "\n";
    }
    file.close();
}

void export_matchings(const Matchings &matchings, std::string filename /*= "result"*/) {
    filename = "results/" + filename;
    std::filesystem::create_directories(std::filesystem::path(filename).parent_path());
    export_matchings_to_tikz(matchings, filename + ".tex");
    export_matchings_to_txt(matchings, filename + ".txt");
    std::cout << "Matchings stored to the file " << filename << ".*\n";
}

Matchings import_matchings(std::string filename) {
    std::ifstream file(filename, std::ios::in);
    if(!file.is_open()) {
        std::cout << "Failed to open " << filename << '\n';
        abort();
    }

    Matchings matchings;
    while(file.peek() != EOF) {
        Matching m;
        for(vertex u = 0; u < VERTICES; u++) {
            int v;
            file >> v;
            m.set(u, v);
        }
        m.verify();
        matchings.push_back(m);
    }
    file.close();
    return matchings;
}
