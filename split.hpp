#pragma once

#include <cassert>
#include <iostream>
#include <vector>

#include "common.hpp"
#include "automorphism.hpp"
#include "extend_all_matchings.hpp"
#include "matching.hpp"
#include "paths.hpp"

// This class allows computing unextendable matchings by building the matching successively, first in the main part (MATCH vertices) and then in the separated part (FORBIDDEN vertices).
class Split {
    const AutoGroup autogroup;
    Matching main_part;
    const std::vector<vertex> separated;

public:
    Split(const Matching &_main_part, const std::vector<vertex> &_separated, const AutoGroup &_autogroup) :
            autogroup(_autogroup), main_part(_main_part), separated(_separated) {
        assert(std::is_sorted(separated.begin(), separated.end()) && std::adjacent_find(separated.begin(), separated.end()) == separated.end());
        assert(separated.back() < VERTICES);
        for(auto u : separated)
            assert(main_part.get(u) == FORBIDDEN);
        assert(main_part.get(0) == FORBIDDEN);
        assert(main_part.count(FORBIDDEN) == separated.size() + 1);
        assert(main_part.count(MATCH) == VERTICES - separated.size() - 1);
    }

    // first match vertices labeled as MATCH
    Matchings get_all_unextendable_matchings_in_main_part(bool odd_number_of_edges_most_significant_direction = false) {
        // generate all possible subsets of vertices that will NOT be matched (remain UNCOVERED)
        Matchings unextendable, matchings = main_part.get_all_subsets_of_vertices(UNCOVERED, false);
        std::cout << "The number of subsets of uncovered vertices is " << matchings.size() << "\n";

        autogroup.remove_isomorphic_matchings(matchings);
        std::cout << "The number of non-isomorphic subsets of uncovered vertices is " << matchings.size()  << "\n";

        // only keep maximal matchings (i.e., no two UNCOVERED vertices may be neighbors)
        filter(matchings, [](const Matching &m) { return m.is_maximal(); });
        std::cout << "The number of maximal subsets of uncovered vertices is " << matchings.size()  << "\n";

        if(odd_number_of_edges_most_significant_direction) {
            filter(matchings, [](const Matching &m) { return m.has_odd_number_of_edges_in_the_most_significant_direction(); });
            std::cout << "The number of subsets of with odd number of covered vertices in Q^{d-1}_0 is " << matchings.size()  << "\n";
        }

        size_t done_matchings = 0, tested_matchings = std::accumulate(matchings.begin(), matchings.end(), size_t(0), [](size_t s, const Matching &m){ return s + m.get_number_of_perfect_matchings_on_match_vertices(); });
        std::cout << "The number of matchings to be tested for extension to a cycle is at most " << tested_matchings << "\n";

        // within this loop the MATCH vertices will actually be matched
        for(const Matching &m : matchings) {
            auto id = &m - matchings.data();
            if(print_progress(id))
                std::cout << "Batch #" << id << " (" << 100*id/matchings.size() << "%), computed matchings " << 100*done_matchings/tested_matchings << "%, number of unextendable " << unextendable.size() << ", extended paths " << Paths<true>::counter_extensions << ", cycles "  << Paths<false>::counter_extensions << ", recursions " << Paths<false>::counter_recursions + Paths<true>::counter_recursions << ", time " << get_running_time() << "\n";
            done_matchings += m.get_number_of_perfect_matchings_on_match_vertices();

            // compute all matchings
            ExtendAllMatchings::run(m, autogroup.create_subgroup_preserving_matchings(m), unextendable, false);
        }
        std::cout << "The number of unextendable matchings is " << unextendable.size() << "\n";

        autogroup.remove_isomorphic_matchings(unextendable);
        std::cout << "The number of non-isomorphic unextendable matchings is " << unextendable.size() << "\n";

        return unextendable;
    }

    // then match terminal vertices in the main part to separated vertices, and separated vertices among each other
    Matchings get_all_unextendable_matchings_between_parts(Matchings &matchings, bool skip_first) {
        Matchings unextendable;
        matchings = Matching::extend_matchings_between_terminals_and_separated(matchings, separated, autogroup, false);
        for(int edge = 1; !matchings.empty(); edge++) {
            matchings = Matching::remove_one_edge_from_all_matchings(matchings);
            std::cout << "The number of matchings after removing " << edge << " edges is " << matchings.size() << "\n";

            if(skip_first && edge == 1)  // remove at least two edges
                continue;

            if(edge > 1) {
                filter(matchings, [](const Matching &m){ return !Paths<true>::is_extendable(m); });
                std::cout << "The number of unextendable matchings after removing " << edge << " edges is " << matchings.size() << "\n";

                autogroup.remove_isomorphic_matchings(matchings);
                std::cout << "The number of non-isomorphic matchings is " << matchings.size() << "\n";
            }

            // match separated vertices
            get_unextendable_matching_in_separated(Matching::extend_matchings_between_terminals_and_separated(matchings, separated, autogroup, true), unextendable);
        }
        return unextendable;
    }

    // match separated vertices among each other
    void get_unextendable_matching_in_separated(Matchings &&matchings, Matchings &unextendable) {
        for(Matching &matching : matchings) {
            auto id = &matching - matchings.data();
            if(print_progress(id))
                std::cout << "Batch #" << id << " (" << 100*id/matchings.size() << "%), number of unextendable " << unextendable.size() << ", extended paths " << Paths<true>::counter_extensions << ", cycles "  << Paths<false>::counter_extensions << ", recursions " << Paths<false>::counter_recursions + Paths<true>::counter_recursions << ", time " << get_running_time() << "\n";

            for(vertex u : separated)  // change status of separated vertices from FORBIDDEN to MATCH
                if(matching.get(u) == FORBIDDEN)
                    matching.set(u, MATCH);
            Matchings separated_matchings = matching.get_all_subsets_of_separated_vertices(separated);
            autogroup.remove_isomorphic_matchings(separated_matchings);
            for(const Matching &m : separated_matchings)
                ExtendAllMatchings::run(m, AutoGroup(), unextendable, false);
        }
    }

    // return true whenever n has k decimal digits and is a multiple of 10^(k-1)
    bool print_progress(size_t n) {
        if(n == 0)
            return true;
        while(n % 10 == 0)
            n /= 10;
        return n <= 10;
    }
};


